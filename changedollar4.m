% Timothy Lincoln   
% 8 - 31 - 2015
% This program determines the number of ways you can create n cents with pennies nickles dimes quarters half dollars and dollars.

% N = number of ways making n cents with pennies and nickles
% D = number of ways using pennies nickles and dimes.
% Q= number of ways using pennies nickles dimes and quarters.
% H= number of ways using pennies nikcles dimes and half dollars.
% F = "                                                         " and full dollars 

% There is only one way to make an amount with pennies
% The number of ways you can make an amount that is less than the amount of the coin is 0 

n= 200 ; %  number of cents e.g. for 1 dollar n = 100  
 No = ones(1,n/5); % place holder for nickles
 N = ones(1,n/5)   ; 
 D = zeros(1,n/5) ;  
 Do= zeros(1, n/5); % place holder for dime loop
 
Q = D  ;
H = D;
F = D;

No = ones(1,n/5);
Qo = Do  ;   % place holder for quarter loop
Ho   = Do ;  % place holder for half dollar loop
Fo = Do  ;  % place holder for dollar loop
%======================================================================
% Determining the number of pennys, nickles and dimes required to make n cents
for(i =1 : n/5);
 N(1, i) = 1 + No(1,i) ;       %note the 1 stands for the number of ways you can make 5 cents with pennys and No will be the value you can make 5 cents with nickles but later on acts as a place holder
 No(1,i+1) = N(1,i);
end

% THe case statements are for the instances when the number of cents n is less than or equal to the value of the coin.
% Determining the number of pennys, nickles and dimes required to make n cents
% i is a variable to advance the loop
for(i =1 : n/5)

  switch i              
  case 1
      D(1,i) = N(1,i);     
      Do(1,1) = D(1,i);
  case 2
      D(1,i) =N(1,i)+1;
      Do(1,2) = D(1,i);
  otherwise
      D(1, i) = N(1,i)+ Do(1,i -2 );
      Do(1,i)= D(1,i);
  end
  end

% determining the number of ways of making n cents with all of the above quarters
for(i = 1:n/5)
    switch i 
     case 1
      Q(1,i) = D(1,i);
      Qo(1,1) = Q(1,i);
    case 2
      Q(1,i) = D(1,i);
      Qo(1,2) = Q(1,i);
    case 3
      Q(1,i) = D(1,i);
      Qo(1,3) = Q(1,i);
   case 4
      Q(1,i) = D(1,i);
      Qo(1,4) = Q(1,i);
   case 5 
      Q(1,i) = D(1,i) +1;
      Qo(1,5) = Q(1,i);
   otherwise
      Q(1, i) = D(1,i)+ Qo(1,i - 5);
      Qo(1,i)= Q(1,i);
    end
end

% determining the number of ways of making n cents with all of the above including half dollars
for(i = 1:n/5)    
     switch i 
     case 1
          H(1,i) = Q(1,i);
          Ho(1,1) = H(1,i);
     case 2
         H(1,i) = Q(1,i);
         Ho(1,2) = H(1,i);
     case 3
         H(1,i) = Q(1,i);
         Ho(1,3) = H(1,i);
     case 4
         H(1,i) = Q(1,i);
         Ho(1,4) = H(1,i);
     case 5
         H(1,i) = Q(1,i);
         Ho(1,5) = H(1,i);
     case 6
         H(1,i) = Q(1,i);
         Ho(1,6) = H(1,i);
     case 7
         H(1,i) = Q(1,i);
         Ho(1,7) = H(1,i);
     case 8
         H(1,i) = Q(1,i);
         Ho(1,8) = H(1,i);
     case 9
         H(1,i) = Q(1,i);
         Ho(1,9) = H(1,i);
     case 10
        H(1,i) = Q(1,i) + 1;
        Ho(1,10) = H(1,i);
     otherwise  
      H(1, i) = Q(1,i)+ Ho(1,i - 10 );
      Ho(1,i)= H(1,i);
     end
end

% determining the number of ways of making n cents with all of the above including dollars
  for(i = 1: n/5)    
     switch i 
     case 1
          F(1,i) = H(1,i);
          Fo(1,1) = F(1,i);
     case 2
          F(1,i) = H(1,i);
          Fo(1,2) = F(1,i);
     case 3
          F(1,i) = H(1,i);
          Fo(1,3) = F(1,i);
     case 4
          F(1,i) = H(1,i);
          Fo(1,4) = F(1,i);
     case 5
          F(1,i) = H(1,i);
          Fo(1,5) = F(1,i);
     case 6
          F(1,i) = H(1,i);
          Fo(1,6) = F(1,i);
     case 7
          F(1,i) = H(1,i);
          Fo(1,7) = F(1,i);
     case 8
          F(1,i) = H(1,i);
          Fo(1,8) = F(1,i);
     case 9
          F(1,i) = H(1,i);
          Fo(1,9) = F(1,i);
     case 10
          F(1,i) = H(1,i);
          Fo(1,10) = F(1,i);
     case 11
          F(1,i) = H(1,i);
          Fo(1,11) = F(1,i);
     case 12
          F(1,i) = H(1,i);
          Fo(1,12) = F(1,i);
     case 13
          F(1,i) = H(1,i);
          Fo(1,13) = F(1,i);
     case 14
          F(1,i) = H(1,i);
          Fo(1,14) = F(1,i);
     case 15
          F(1,i) = H(1,i);
          Fo(1,15) = F(1,i);
     case 16
          F(1,i) = H(1,i);
          Fo(1,16) = F(1,i);
     case 17
          F(1,i) = H(1,i);
          Fo(1,17) = F(1,i);
     case 18
          F(1,i) = H(1,i);
          Fo(1,18) = F(1,i);
     case 19
          F(1,i) = H(1,i);
          Fo(1,19) = F(1,i);
     case 20
          F(1,i) = H(1,i) +1;
          Fo(1,20) = F(1,i);
     otherwise 
      F(1, i) = H(1,i)+ Fo(1,i - 20); % go back to the original amount(Fo(1-20 )) and add it 
      Fo(1,i)= F(1,i);
     end
 end 

 fprintf('the number of ways to make %d cents from pennies nickles dimes quarters half dollars and dollars is    ' , n)
 disp(F(1, n/5))